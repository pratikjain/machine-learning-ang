function [C, sigma] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and
%   sigma. You should complete this function to return the optimal C and
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.2;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example,
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using
%        mean(double(predictions ~= yval))
%

Cval = [0.01 0.05 0.11 0.2 0.5 0.7 1.3 1.7 2 5 10 40]';
Sval = [0.01 0.05 0.11 0.2 0.5 0.7 1.3 1.7 2 5 10 40]';
maxError = 10000;

for it1 = 1: length(Cval),
	for it2 = 1:length(Sval),
		model = svmTrain(X, y, Cval(it1), @(X, y) gaussianKernel(X, y, Sval(it2)));
		predictions = svmPredict(model, Xval);
		predictionError = mean(double(predictions ~= yval));
		fprintf("Error: %f \n", predictionError);
		if predictionError < maxError,
			maxError = predictionError;
			C = Cval(it1);
			sigma = Sval(it2);
	end;
end;
% =========================================================================

end
